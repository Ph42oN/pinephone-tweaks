#!/bin/bash


# Change this to your internet connection name, username and user UID
#####################################################################
CON_NAME="Postpaid (contract) NAT (available for all subscribers)"
USERNAME="alarm"
USER_UID="1000"

reactivate_modem() {
        # Check for ongoing calls 
        ###################################
        if [[ -n "$(mmcli -m any --voice-list-calls 2>&1|grep 'No calls\|error:')" ]]; then

        # Check if proper modem interface exists
        ########################################
        sleep 5
        if [[ -z "$(nmcli d | grep cdc-wdm0)" ]]; then

            # If not, then check for ttyUSB. If it doesn't exist then wait for it to appear
            ###############################################################################
            while ! nmcli d | grep ttyUSB >> /dev/null;
            do 
            echo "No ttyUSB - waiting for the interface to appear..."
            sleep 2
            done

            # Eject modem
            ##############################
            echo '3-1' |tee /sys/bus/usb/drivers/usb/unbind && echo "Ejecting modem..."
            
            # Wait for a few seconds
            #############################
            sleep 3
            
            # Insert modem
            ##############################
            echo '3-1' |tee /sys/bus/usb/drivers/usb/bind && echo "Inserting modem..."

            # Wait for a few seconds
            ##############################
            sleep 3

        fi

        # Check if mobile data is active
        ##################################
        if [[ "$(nmcli d | grep cdc-wdm0 | awk '{ print $NF }')" == "--" ]]; then
        
        # Try to bring connection up ad monitor
        #######################################
	while ! nmcli d |grep cdc-wdm0|grep connected;
        do
        echo "Trying to bring Mobile Data $CON_NAME up..."
        nmcli c up "$CON_NAME"
        sleep 5
        done     
        echo "Modem back online and Mobile Data connected..." 

        fi
    fi
}


# Start monitoring DBUS for screen lock/unlock events and act accordingly
#############################################################################
sudo -u $USERNAME dbus-monitor --address "unix:path=/run/user/$USER_UID/bus" "type='signal',interface='org.gnome.ScreenSaver'" |

  while read x; do
    case "$x" in 
      *"boolean true"*) echo SCREEN_LOCKED;;

      *"boolean false"*) echo SCREEN_UNLOCKED && reactivate_modem;;

    esac
  done

