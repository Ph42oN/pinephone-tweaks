#!/bin/bash
echo "installing custom squeekboard terminal layout"
mkdir -p ~/.local/share/squeekboard/keyboards
cp terminal.yaml ~/.local/share/squeekboard/keyboards/
echo "copying phoc.ini"
sudo mkdir /etc/phosh
sudo cp phoc.ini /etc/phosh/

#echo "installing mguard service"
#sudo cp mguard_service.sh /usr/bin/mguard_service.sh
#sudo cp mguard.service /etc/systemd/system/mguard.service
#sudo chmod 644 /etc/systemd/system/mguard.service
#echo "enabling mguard service"
#sudo systemctl daemon-reload
#sudo systemctl enable mguard
#sudo systemctl start mguard

echo "setting battery sleep timetout to 60 seconds and connected to 5 min"
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout 60
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 300

echo "setting dark gtk theme and disabling animations"
gsettings set org.gnome.desktop.interface gtk-theme Adwaita-dark
gsettings set org.gnome.desktop.interface enable-animations false
