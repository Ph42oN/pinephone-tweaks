#include <iostream>
#include <fstream>
#include <fcntl.h>
#include <unistd.h>
#include <cstdlib>
#include <thread>
#include <chrono>
#include <X11/Xlib.h>
#include <X11/extensions/scrnsaver.h>

struct input_event {
	struct timeval time;
	unsigned short type;
	unsigned short code;
	unsigned int value;
};

bool sleeping = false;
bool brChange = false;
const int softsleeptime=60*1000;
const int suspendtime=300*1000;
const std::string user="alarm";
const std::string eventdev = "/dev/input/event0";
const std::string tsid = "7";
std::string cmd;

int readevent(std::string devname,ushort code) {
	struct input_event inp;
	int dev;

	dev = open(devname.c_str(), O_RDONLY);
    if(!dev) {
	   	std::cerr << "Could not open "<<devname;
	  	return -1;
	} else {
		read(dev, &inp, sizeof(inp));
		if (inp.code == code) {
			//printf("key:%i state:%i\n", inp.code, inp.value);
			close(dev);
			return inp.value;
		}
		return -2;
	}
}

int writeFile(std::string filename, std::string value) {
	std::ofstream file;

	file.open(filename);
	if(!file.is_open()) {
		std::cerr << "Could not open " << filename << "\n";
		return -1;
	} else {
		file << value;
		file.close();
		return 1;
	}
}

void PPSleep() {
	if (!sleeping) {
		sleeping = true;
		writeFile("/sys/devices/system/cpu/cpu1/online", "0");
		writeFile("/sys/devices/system/cpu/cpu2/online", "0");
		writeFile("/sys/devices/system/cpu/cpu3/online", "0");

		cmd="sudo -u "+user+" xinput disable "+tsid;
		system(cmd.c_str());
		cmd="sudo -u "+user+" xset dpms force off";
		system(cmd.c_str());
		writeFile("/sys/class/leds/blue:indicator/brightness", "1");
	}
	//usleep(200 * 1000);
	//writeFile("/sys/power/state", "mem");
}

void PPWake() {
	if(sleeping) {
		sleeping = false;
		writeFile("/sys/devices/system/cpu/cpu1/online", "1");
		writeFile("/sys/devices/system/cpu/cpu2/online", "1");
		writeFile("/sys/devices/system/cpu/cpu3/online", "1");

		cmd="sudo -u "+user+" xinput enable "+tsid;
                system(cmd.c_str());
                cmd="sudo -u "+user+" xset dpms force on";
                system(cmd.c_str());
		writeFile("/sys/class/leds/blue:indicator/brightness", "0");
	}
}

void readkeys() {
	while(1) {
		if (readevent(eventdev,116) == 1) {
			std::cout << "Power button pressed\n";
			if(sleeping) {
				PPWake();
			} else {
				PPSleep();
				std::this_thread::sleep_for(std::chrono::milliseconds(200));
				writeFile("/sys/power/state", "mem");
				std::this_thread::sleep_for(std::chrono::milliseconds(200));
				PPWake();
			}
		}
	}
}

int main(void) {
	setenv("DISPLAY", ":0", true);
	Display *display = XOpenDisplay(":0");
	XScreenSaverInfo *info = XScreenSaverAllocInfo();
	//int event_base, error_base;
	std::cout << "init done\n";

	std::thread keythread([=] { readkeys(); });
	while (1) {
		XScreenSaverQueryInfo(display, DefaultRootWindow(display), info);
  		//std::cout << info->idle <<" ms\n";


		if(info->idle >= softsleeptime) {
			if(!sleeping) {
				std::cout << "Soft sleep time reached\n";
				PPSleep();
			}
		}
		if(info->idle >= suspendtime) {
			std::cout << "Suspend time reached\n";
            PPSleep();
			std::this_thread::sleep_for(std::chrono::milliseconds(200));
			writeFile("/sys/power/state", "mem");
			
			std::this_thread::sleep_for(std::chrono::milliseconds(200));
			PPWake();
        }
		
		//std::cout << "waiting 10? sec\n";
		std::this_thread::sleep_for(std::chrono::seconds(10));
	}
	if (keythread.joinable()) {
		keythread.join();
	}
	return 0;
}
